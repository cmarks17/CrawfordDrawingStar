import turtle as T


def instructions():
    print("Pick a shape to draw")
    print("Type 1 for star")
    print("Type 2 for triangle")
    print("Type 3 for square")
    print("Type 4 for pentagon")
    print("Type 0 to quit")
    pick()


def pick():
    userinput = input()
    print("user input {}".format(userinput))
    if userinput == "1":
        Star()
    elif userinput == "2":
        Triangle()
    elif userinput == "3":
        Square()
    elif userinput == "4":
        Pentagon()
    elif userinput == "0":
        Quit()
    else:
        print("You didn't input a valid choice")

def Star():
    T.Screen()
    T.forward(100)
    T.left(216)
    T.forward(100)
    T.left(72)
    T.forward(100)
    T.left(216)
    T.forward(100)
    T.left(72)
    T.forward(100)
    T.left(216)
    T.forward(100)
    T.left(72)
    T.forward(100)
    T.left(216)
    T.forward(100)
    T.left(72)
    T.forward(100)
    T.left(216)
    T.forward(100)
    T.done
    T.reset
    Newshape()
'''
    tar(2, "red")
    tar(3, "blue")
    T.right(180)
    tar(4, "green")
    T.right(180)
    tar(5, "orange")
    T.right(180)
    tar(6, "brown")
    T.right(180)
    tar(7, "yellow")
    T.right(180)
    tar(8, "blue")
    T.right(180)
    tar(9, "blue")
    T.right(180)
    tar(10, "blue")
    T.right(180)
    tar(11, "blue")
    T.right(180)
    tar(12, "blue")
    T.right(180)
    tar(13, "blue")
    T.right(180)
    tar(14, "blue")
    T.right(180)
    tar(15, "blue")
    T.right(180)
    tar(16, "blue")
    T.right(180)
    tar(17, "blue")
    T.right(180)
    tar(18, "blue")
    T.right(180)
    tar(19, "blue")
    T.right(180)
    T.done
    '''


def Triangle():
    T.Screen
    T.forward(100)
    T.left(120)
    T.forward(100)
    T.left(120)
    T.forward(100)
    T.done
    T.reset
    Newshape()

def Square():
    T.Screen
    T.forward(100)
    T.left(90)
    T.forward(100)
    T.left(90)
    T.forward(100)
    T.left(90)
    T.forward(100)
    T.done
    T.reset
    Newshape()

def Pentagon():
    T.Screen
    T.forward(100)
    T.left(288)
    T.forward(100)
    T.left(288)
    T.forward(100)
    T.left(288)
    T.forward(100)
    T.left(288)
    T.forward(100)
    T.done
    Newshape()

def Quit():
    print("thanks for drawing")
    quit
    pass

def Newshape():
    print("Would you like to draw another shape?")
    print("Y/N")
    userinput = input()
    if userinput == "Y" or userinput == "y":
        instructions()
    if userinput == "N" or userinput == "n":
        pass


instructions()